import request from "@/utils/request";
import { AUTHORIZE_URLS } from "@/helpers/urls";

export function login(data) {
  return request({
    // returnFullResponse: true,
    // popupErrorMessage: false,
    url: AUTHORIZE_URLS.LOGIN_URL,
    method: "post",
    data
  });
}
export function register(data) {
  return request({
    // returnFullResponse: true,
    // popupErrorMessage: false,
    url: AUTHORIZE_URLS.REGISTER_URL,
    method: "post",
    data
  });
}

export function getAuthorizedUser() {
  return request({
    url: AUTHORIZE_URLS.GET_AUTHORIZED_USER,
    method: "get"
  });
}

export function logout() {
  return request({ url: "/user/logout", method: "post" });
}
