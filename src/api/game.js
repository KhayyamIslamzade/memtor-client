import request from "@/utils/request";
import { GAME_URLS } from "@/helpers/urls";

export function updateGameSetting(data) {
  return request({
    url: GAME_URLS.UPDATE_GAME_SETTING_URL,
    method: "put",
    data
  });
}
export function get() {
  return request({
    url: GAME_URLS.GET_URL,
    method: "get"
  });
}
export function pass(id) {
  return request({
    url: GAME_URLS.PASS_GAME_URL(id),
    method: "put"
  });
}
export function checkAnswer(data) {
  return request({
    url: GAME_URLS.CHECK_ANSWER_URL,
    method: "post",
    data
  });
}
export function getGameSetting() {
  return request({
    url: GAME_URLS.GET_GAME_SETTING,
    method: "get"
  });
}
