import request from "@/utils/request";
import { GROUP_URLS } from "@/helpers/urls";
export function create(data) {
  return request({
    url: GROUP_URLS.CREATE_URL,
    method: "post",
    data
  });
}
export function update(data) {
  return request({
    url: GROUP_URLS.UPDATE_URL,
    method: "put",
    data
  });
}
export function get(id) {
  return request({
    url: GROUP_URLS.GET_URL(id),
    method: "get"
  });
}
export function getAll(params) {
  return request({
    url: GROUP_URLS.GET_ALL_URL,
    method: "get",
    params
  });
}
export function remove(id) {
  return request({
    url: GROUP_URLS.REMOVE_URL(id),
    method: "delete"
  });
}
