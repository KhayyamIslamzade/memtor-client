import request from "@/utils/request";
import { SESSION_URLS } from "@/helpers/urls";
export function create(data) {
  return request({
    url: SESSION_URLS.CREATE_URL,
    method: "post",
    data
  });
}
export function update(data) {
  return request({
    url: SESSION_URLS.UPDATE_URL,
    method: "put",
    data
  });
}
export function start(id) {
  return request({
    url: SESSION_URLS.START_URL(id),
    method: "put"
  });
}
export function close(id) {
  return request({
    url: SESSION_URLS.CLOSE_URL(id),
    method: "put"
  });
}
export function get(id) {
  return request({
    url: SESSION_URLS.GET_URL(id),
    method: "get"
  });
}
export function getSessionActiveGame(id) {
  return request({
    url: SESSION_URLS.GET_SESSION_ACTIVE_GAME_URL(id),
    method: "get"
  });
}
export function getAll(params) {
  return request({
    url: SESSION_URLS.GET_ALL_URL,
    method: "get",
    params
  });
}
export function remove(id) {
  return request({
    url: SESSION_URLS.REMOVE_URL(id),
    method: "delete"
  });
}
