import request from "@/utils/request";
import { USER_URLS } from "@/helpers/urls";

export function getUsers(params) {
  if (!_.isUndefined(params)) {
    if (_.isUndefined(params.isAll)) {
      params["isAll"] = true;
    }
  } else {
    params = { isAll: true };
  }
  return request({
    url: USER_URLS.GET_ALL_URL,
    method: "get",
    params: params
  });
}
export function getUserInfos() {
  return request({
    url: USER_URLS.GET_INFOS_URL,
    method: "get"
  });
}

export function getUser(id) {
  return request({
    url: USER_URLS.GET_URL + "/" + id,
    method: "get"
  });
}

export function addUser(data) {
  return request({ url: USER_URLS.ADD_URL, method: "post", data });
}

export function editUser(data) {
  return request({ url: USER_URLS.EDIT_URL, method: "post", data });
}
export function changePassword(data) {
  return request({
    url: USER_URLS.CHANGE_PASSWORD_URL,
    method: "post",
    data
  });
}
export function deleteUser(id) {
  return request({
    url: USER_URLS.DELETE_URL + "/" + id,
    method: "post"
  });
}
