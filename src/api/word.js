import request from "@/utils/request";
import { WORD_URLS } from "@/helpers/urls";
export function create(data) {
  return request({
    url: WORD_URLS.CREATE_URL,
    method: "post",
    data
  });
}
export function createRange(data) {
  return request({
    url: WORD_URLS.CREATE_RANGE_URL,
    method: "post",
    data
  });
}
export function update(data) {
  return request({
    url: WORD_URLS.UPDATE_URL,
    method: "put",
    data
  });
}
export function get(id) {
  return request({
    url: WORD_URLS.GET_URL(id),
    method: "get"
  });
}
export function getAll(params) {
  return request({
    url: WORD_URLS.GET_ALL_URL,
    method: "get",
    params
  });
}
export function remove(id) {
  return request({
    url: WORD_URLS.REMOVE_URL(id),
    method: "delete"
  });
}
export function removeList(data) {
  return request({
    url: WORD_URLS.REMOVE_LIST_URL,
    method: "delete",
    data
  });
}
