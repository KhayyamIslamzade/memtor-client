export const gameTypeOptions = [
  {
    key: 3,
    value: "Random"
  },
  {
    key: 1,
    value: "Meaning Game"
  },
  {
    key: 2,
    value: "Completion Game"
  }
];
export const gameLevelOptions = [
  {
    key: 4,
    value: "Random"
  },
  {
    key: 1,
    value: "Easy"
  },
  {
    key: 2,
    value: "Medium"
  },
  {
    key: 3,
    value: "Hard"
  }
];
export const sessionStatusOptions = [
  {
    key: 1,
    value: "Pending"
  },
  {
    key: 2,
    value: "Started"
  },
  {
    key: 3,
    value: "Closed"
  }
];
