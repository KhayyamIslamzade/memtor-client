export function getRolePermissions(roles) {
  let permissions = [];
  roles.forEach(role => {
    permissions = permissions.concat(role.permissions);
  });
  return permissions;
}
export function getRoleNames(roles) {
  return roles.map(c => c.name);
}

export function hasRole(roles, route) {
  if (isPermissionOrRoleMetaExist(route)) {
    if (isRoleMetaExist(route))
      return roles && roles.some(role => requiredRoles.includes(role.name));
    return false;
  } else {
    return true;
  }
}
export function isPermissionOrRoleMetaExist(route) {
  return (
    isRoleMetaExist(route) ||
    isPermissionMetaExist(route) ||
    isDirectivePermissionMetaExist(route)
  );
}
export function isRoleMetaExist(route) {
  return route.meta && route.meta.roles && route.meta.roles.length > 0;
}
export function isPermissionMetaExist(route) {
  return (
    route.meta && route.meta.permissions && route.meta.permissions.length > 0
  );
}
export function isDirectivePermissionMetaExist(route) {
  return (
    route.meta &&
    route.meta.directivePermissions &&
    route.meta.directivePermissions.length > 0
  );
}
export function hasPermission(permissions, route) {
  if (isPermissionOrRoleMetaExist(route)) {
    if (isPermissionMetaExist(route))
      return permissions.some(permission =>
        route.meta.permissions.includes(permission)
      );

    return false;
  } else {
    return true;
  }
}
export function hasDirectivePermission(directivePermissions, route) {
  if (isPermissionOrRoleMetaExist(route)) {
    if (isDirectivePermissionMetaExist(route)) {
      let result = directivePermissions.some(directivePermission =>
        route.meta.directivePermissions.includes(directivePermission)
      );

      return result;
    }

    return false;
  } else {
    return true;
  }
}
