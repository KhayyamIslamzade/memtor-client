export const AUTHORIZE_URLS = {
  LOGIN_URL: "Account/gettoken",
  GET_AUTHORIZED_USER: "Account/GetAuthorizedUser",
  REGISTER_URL: "Account/Register"
};
export const USER_URLS = {
  GET_ALL_URL: "/User/GetAll",
  GET_ALL_WITH_ROLE_PERMISSION_URL: "/User/GetAllWithRolePermission",
  GET_WITH_ROLE_PERMISSION_URL: "/User/GetWithRolePermission",
  GET_ALL_WITH_ROLES_URL: "/UserRole/GetListUserRoles",

  GET_URL: "/User/Get",
  ADD_URL: "/User/Add",
  EDIT_URL: "/User/Edit",
  DELETE_URL: "/User/Delete",
  CHANGE_PASSWORD_URL: "/User/ChangePassword"
};

export const ROLE_URLS = {
  GET_LIST_ROLE_PERMISSIONS_URL: "/RolePermission/GetListRolePermissions",
  GET_ALL_URL: "/Role/GetAll",
  GET_URL: "/Role/Get",
  ADD_URL: "/Role/Add",
  EDIT_URL: "/Role/Edit",
  DELETE_URL: "/Role/Delete"
};
export const GROUP_URLS = {
  GET_ALL_URL: "/Group",
  GET_URL: id => `/Group/${id}`,
  CREATE_URL: "/Group",
  UPDATE_URL: "/Group",
  REMOVE_URL: id => `/Group/${id}`
};
export const TAG_URLS = {
  GET_ALL_URL: "/Tag",
  GET_URL: id => `/Tag/${id}`,
  CREATE_URL: "/Tag",
  UPDATE_URL: "/Tag",
  REMOVE_URL: id => `/Tag/${id}`
};
export const WORD_URLS = {
  GET_ALL_URL: "/Word",
  GET_URL: id => `/Word/${id}`,
  CREATE_URL: "/Word",
  CREATE_RANGE_URL: "/Word/AddList",
  UPDATE_URL: "/Word",
  REMOVE_URL: id => `/Word/${id}`,
  REMOVE_LIST_URL: `/Word`
};
export const GAME_URLS = {
  GET_URL: `/Game`,
  GET_GAME_SETTING: "/Game/GetGameSetting",
  UPDATE_GAME_SETTING_URL: "/Game/GameSetting",
  PASS_GAME_URL: id => `/Game/${id}/Pass`,
  CHECK_ANSWER_URL: `/Game/CheckAnswer`
};
export const SESSION_URLS = {
  GET_ALL_URL: "/Session",
  GET_URL: id => `/Session/${id}`,
  CREATE_URL: "/Session",
  UPDATE_URL: "/Session",
  REMOVE_URL: id => `/Session/${id}`,
  START_URL: id => `/Session/${id}/Start`,
  CLOSE_URL: id => `/Session/${id}/Close`,
  GET_SESSION_ACTIVE_GAME_URL: id => `/Session/${id}/GetActiveGame`
};
