import "./assets/css/vendor/dropzone.min.css";
import "bootstrap-vue/dist/bootstrap-vue.css";
import "./assets/css/vendor/bootstrap.min.css";
import "./assets/css/vendor/bootstrap.rtl.only.min.css";
import "video.js/dist/video-js.css";

import {
  defaultColor,
  themeSelectedColorStorageKey,
  themeColorKey
} from "./constants/config";
/*  if you want use single color theme

- ColorSwitcher component remove in 'src/App.vue' file.
- Remove multicolor section in current file
- Uncomment the two lines below

import "./assets/css/sass/themes/piaf.light.blueolympic.scss";
import "./main";
*/
var color = defaultColor;
if (localStorage.getItem(themeColorKey)) {
  color = localStorage.getItem(themeColorKey);
}
let isDark = color.includes("dark");

if (isDark) {
  require("./assets/css/sass/themes/piaf.dark.blueolympic.scss");
} else {
  require("./assets/css/sass/themes/piaf.light.blueolympic.scss");
}
import "./main";
document.body.classList.add("rounded");
/* if you want single color remove this section:multicolor */
// var color = defaultColor;

// if (localStorage.getItem(themeSelectedColorStorageKey)) {
//   color = localStorage.getItem(themeSelectedColorStorageKey);
// }
// let render = () => {
//   import("./assets/css/sass/themes/piaf." + color + ".scss").then(() =>
//     require("./main")
//   );
// };

// render();
/* if you want single color remove this section:multicolor */
