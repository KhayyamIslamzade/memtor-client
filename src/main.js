import Vue from "vue";
import App from "./App";

// BootstrapVue add
import BootstrapVue from "bootstrap-vue";
// Router & Store add
import router from "./router";
import store from "./store";
// Multi Language Add
import en from "./locales/en.json";
import es from "./locales/es.json";
import VueI18n from "vue-i18n";
import {
  defaultLocale,
  localeOptions
  // firebaseConfig
} from "./constants/config";
// Notification Component Add
import Notifications from "./components/Common/Notification";
// Breadcrumb Component Add
import Breadcrumb from "./components/Common/Breadcrumb";
// RefreshButton Component Add
import RefreshButton from "./components/Common/RefreshButton";
// Colxx Component Add
import Colxx from "./components/Common/Colxx";
// Perfect Scrollbar Add
import vuePerfectScrollbar from "vue-perfect-scrollbar";
import contentmenu from "v-contextmenu";
import VueLineClamp from "vue-line-clamp";
import VueScrollTo from "vue-scrollto";

import "./permission"; // permission control
import "./prototypes"; // add js prototypes

// Vue SweetAlert
import "sweetalert2/dist/sweetalert2.min.css";
import VueSweetalert2 from "vue-sweetalert2";

import "vue-select/dist/vue-select.css";
import vSelect from "vue-select";

import "@voerro/vue-tagsinput/dist/style.css";
import VoerroTagsInput from "@voerro/vue-tagsinput";
// import firebase from "firebase/app";
// import "firebase/auth";

Vue.use(BootstrapVue);
Vue.use(VueSweetalert2);
Vue.use(VueI18n);
const messages = { en: en, es: es };
const locale =
  localStorage.getItem("currentLanguage") &&
  localeOptions.filter(x => x.id === localStorage.getItem("currentLanguage"))
    .length > 0
    ? localStorage.getItem("currentLanguage")
    : defaultLocale;
const i18n = new VueI18n({
  locale: locale,
  fallbackLocale: "en",
  messages
});
Vue.use(Notifications, { duration: 3000, permanent: false });
Vue.use(require("vue-shortkey"));
Vue.use(contentmenu);
Vue.use(VueScrollTo);
Vue.use(VueLineClamp, {
  importCss: true
});
Vue.component("v-select", vSelect);
Vue.component("tags-input", VoerroTagsInput);
Vue.component("piaf-breadcrumb", Breadcrumb);
Vue.component("b-refresh-button", RefreshButton);
Vue.component("b-colxx", Colxx);
Vue.component("vue-perfect-scrollbar", vuePerfectScrollbar);

// firebase.initializeApp(firebaseConfig);
Vue.config.productionTip = false;

export default new Vue({
  i18n,
  router,
  store,
  render: h => h(App)
}).$mount("#app");
