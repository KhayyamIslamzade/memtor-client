String.prototype.replaceAt = function(index, replacement) {
  return (
    this.substr(0, index) +
    replacement +
    this.substr(index + replacement.length)
  );
};
String.prototype.indexOfChars = function(c) {
  var indices = [];
  for (var i = 0; i < this.length; i++) {
    if (this[i] === c) indices.push(i);
  }
  return indices;
};
if (!String.format) {
  String.format = function(format) {
    var args = Array.prototype.slice.call(arguments, 1);
    return format.replace(/{(\d+)}/g, function(match, number) {
      return typeof args[number] != "undefined" ? args[number] : match;
    });
  };
}
