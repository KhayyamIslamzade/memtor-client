const getters = {
  roles: state => state.user.roles,
  directivePermissions: state => state.user.directivePermissions,
  currentUser: state => state.user.currentUser,
  topNavbarRoutes: state => state.permission.topNavbarRoutes
};
export default getters;
