import { login, getAuthorizedUser, register } from "@/api/auth";
import { getToken, setToken, removeToken } from "@/utils/auth";
import router, { resetRouter } from "@/router";
const state = {
  directivePermissions: [],
  user: {},
  currentUser: {},
  roles: []
};

const mutations = {
  SET_CURRENT_USER: (state, payload) => {
    const { id, email, userName } = payload;
    state.currentUser = { id, email, userName };
  },
  SET_DIRECTIVE_PERMISSIONS: (state, payload) => {
    state.directivePermissions = payload;
  },
  SET_INTRODUCTION: (state, payload) => {
    state.introduction = payload;
  },

  SET_AVATAR: (state, payload) => {
    state.avatar = payload;
  },
  SET_ROLES: (state, payload) => {
    state.roles = payload;
  }
};

function mapRoleObj(roles) {
  let obj = roles.map(role => {
    let name = role.name.toLowerCase();
    let permissions = [];
    if (role.permissions && role.permissions.length > 0)
      permissions = role.permissions.map(item => {
        return `${item.category.label}_${item.permission.label}`.toLowerCase();
      });
    return { name, permissions };
  });

  return obj;
}
function mapPermissionObj(permissions) {
  if (permissions && permissions.length > 0)
    return permissions.map(item => {
      return item.label.toLowerCase();
    });
  else return [];
}

const actions = {
  // user login
  login({ commit }, data) {
    const { emailOrUsername, password } = data;

    return new Promise((resolve, reject) => {
      login({ emailOrUsername: emailOrUsername.trim(), password: password })
        .then(response => {
          const { token } = response;

          setToken(token);
          resolve(response);
        })
        .catch(error => {
          reject(error);
        });
    });
  },

  register({ commit, dispatch }, data) {
    return new Promise((resolve, reject) => {
      register(data)
        .then(() => {
          const { email, password } = data;
          dispatch("login", {
            emailOrUsername: email.trim(),
            password: password
          }).then(response => {
            resolve(response);
          });
        })
        .catch(error => {
          reject(error);
        });
    });
  },

  getAuthorizedUser({ commit, state }) {
    return new Promise((resolve, reject) => {
      getAuthorizedUser()
        .then(response => {
          if (!response) {
            reject("Verification failed, please Login again.");
          }

          let { roles, directivePermissions, userName } = response;

          // roles must be a non-empty array
          if (!roles || roles.length <= 0) {
            roles = [{ name: "none", permissions: [] }];
          }

          if (!roles || roles.length <= 0) {
            reject("getAuthorizedUser: roles must be a non-null array!");
          }

          let mappedRoles = mapRoleObj(roles);
          let mappedDirectivePermissions = mapPermissionObj(
            directivePermissions
          );

          commit("SET_CURRENT_USER", response);
          commit("SET_ROLES", mappedRoles);
          commit("SET_DIRECTIVE_PERMISSIONS", mappedDirectivePermissions);

          resolve({
            roles: mappedRoles,
            directivePermissions: mappedDirectivePermissions,
            userName
          });
        })
        .catch(error => {
          reject(error);
        });
    });
  },

  // user logout
  logout({ commit, state, dispatch }) {
    commit("SET_ROLES", []);
    commit("SET_CURRENT_USER", []);
    commit("SET_DIRECTIVE_PERMISSIONS", []);
    removeToken();
    resetRouter();
  },

  // remove token
  resetToken({ commit }) {
    return new Promise(resolve => {
      commit("SET_ROLES", []);
      removeToken();
      resolve();
    });
  },

  // dynamically modify permissions
  changeRoles({ commit, dispatch }, role) {
    return new Promise(async resolve => {
      const token = role + "-token";

      commit("SET_TOKEN", token);
      setToken(token);

      const { roles } = await dispatch("getInfo");

      resetRouter();

      // generate accessible routes map based on roles
      const accessRoutes = await dispatch("permission/generateRoutes", roles, {
        root: true
      });

      // dynamically add accessible routes
      router.addRoutes(accessRoutes);

      // reset visited views and cached views
      dispatch("tagsView/delAllViews", null, { root: true });

      resolve();
    });
  }
};

export default {
  namespaced: true,
  state,
  mutations,
  actions
};
